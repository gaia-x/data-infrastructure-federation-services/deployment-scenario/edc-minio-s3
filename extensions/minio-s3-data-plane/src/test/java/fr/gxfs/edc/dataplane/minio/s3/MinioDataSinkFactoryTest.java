package fr.gxfs.edc.dataplane.minio.s3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

import fr.gxfs.edc.extension.s3.api.MinioConnectorAPIImpl;
import fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema;
import org.eclipse.edc.connector.dataplane.spi.pipeline.DataSink;
import org.eclipse.edc.connector.dataplane.spi.pipeline.DataSource;
import org.eclipse.edc.spi.EdcException;
import org.eclipse.edc.spi.monitor.Monitor;
import org.eclipse.edc.spi.types.domain.DataAddress;
import org.eclipse.edc.spi.types.domain.transfer.DataFlowStartMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class MinioDataSinkFactoryTest {

  private MinioConnectorAPIImpl minioConnectorAPI;
  private Monitor monitor;
  private ExecutorService executorService;

  @BeforeEach
  void setUp() {
    monitor = mock(Monitor.class);
    minioConnectorAPI = mock(MinioConnectorAPIImpl.class);
    executorService = mock(ExecutorService.class);
  }

  @Test
  void canHandleShouldReturnTrueWhenDataFlowStartMessageDestinationDataAddressTypeIsMinioBucketSchemaType() {
    // Given
    MinioDataSinkFactory minioDataSinkFactory = new MinioDataSinkFactory(minioConnectorAPI,
        monitor, executorService);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type("sourceType")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .build())
        .build();

    // When
    boolean result = minioDataSinkFactory.canHandle(dataFlowStartMessage);

    // Then
    assertTrue(result);
  }

  @Test
  void canHandleShouldReturnFalseWhenDataFlowStartMessageDestinationDataAddressTypeIsNotMinioBucketSchemaType() {
    // Given
    MinioDataSinkFactory minioDataSinkFactory = new MinioDataSinkFactory(minioConnectorAPI,
        monitor, executorService);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type("sourceType")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .build())
        .build();

    // When
    boolean result = minioDataSinkFactory.canHandle(dataFlowStartMessage);

    // Then
    assertFalse(result);
  }

  @Test
  void validateRequestShouldReturnSuccessResultWhenDataFlowStartMessageDestinationDataAddressIsValid() {
    // Given
    MinioDataSinkFactory minioDataSinkFactory = new MinioDataSinkFactory(minioConnectorAPI,
        monitor, executorService);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.BUCKET_NAME, "bucketName")
            .property(MinioBucketSchema.OBJECT_PREFIX, "objectPrefix")
            .property(MinioBucketSchema.OBJECT_NAME, "objectName")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.BUCKET_NAME, "destinationBucketName")
            .property(MinioBucketSchema.OBJECT_PREFIX, "objectPrefix")
            .property(MinioBucketSchema.OBJECT_NAME, "objectName")
            .build())
        .build();

    // When
    var result = minioDataSinkFactory.validateRequest(dataFlowStartMessage);

    // Then
    assertTrue(result.succeeded());
  }

  @Test
  void validateRequestShouldReturnFailureResultWhenDataFlowStartMessageDestinationDataAddressPropertyBucketNameIsMissing() {
    // Given
    MinioDataSinkFactory minioDataSinkFactory = new MinioDataSinkFactory(minioConnectorAPI,
        monitor, executorService);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.BUCKET_NAME, "bucketName")
            .property(MinioBucketSchema.OBJECT_PREFIX, "objectPrefix")
            .property(MinioBucketSchema.OBJECT_NAME, "objectName")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.OBJECT_PREFIX, "objectPrefix")
            .property(MinioBucketSchema.OBJECT_NAME, "objectName")
            .build())
        .build();

    // When
    var result = minioDataSinkFactory.validateRequest(dataFlowStartMessage);

    // Then
    assertTrue(result.failed());
    assertEquals(1, result.getFailureMessages().size());
  }

  @Test
  void validateRequestShouldReturnSuccessResultWhenDataFlowStartMessageDestinationDataAddressPropertyObjectNameIsMissing() {
    // Given
    MinioDataSinkFactory minioDataSinkFactory = new MinioDataSinkFactory(minioConnectorAPI,
        monitor, executorService);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.BUCKET_NAME, "bucketName")
            .property(MinioBucketSchema.OBJECT_PREFIX, "objectPrefix")
            .property(MinioBucketSchema.OBJECT_NAME, "objectName")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.BUCKET_NAME, "destinationBucketName")
            .property(MinioBucketSchema.OBJECT_PREFIX, "objectPrefix")
            .build())
        .build();

    // When
    var result = minioDataSinkFactory.validateRequest(dataFlowStartMessage);

    // Then
    assertTrue(result.succeeded());
    assertEquals(0, result.getFailureMessages().size());
  }

  @Test
  void validateRequestShouldReturnSuccessResultWhenDataFlowStartMessageDestinationDataAddressPropertyObjectPrefixIsMissing() {
    // Given
    MinioDataSinkFactory minioDataSinkFactory = new MinioDataSinkFactory(minioConnectorAPI,
        monitor, executorService);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.BUCKET_NAME, "bucketName")
            .property(MinioBucketSchema.OBJECT_PREFIX, "objectPrefix")
            .property(MinioBucketSchema.OBJECT_NAME, "objectName")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.BUCKET_NAME, "destinationBucketName")
            .property(MinioBucketSchema.OBJECT_NAME, "objectName")
            .build())
        .build();

    // When
    var result = minioDataSinkFactory.validateRequest(dataFlowStartMessage);

    // Then
    assertTrue(result.succeeded());
    assertEquals(0, result.getFailureMessages().size());
  }

  @Test
  void validateRequestShouldReturnFailureResultWhenDataFlowStartMessageDestinationDataAddressIsInvalid() {
    // Given
    MinioDataSinkFactory minioDataSinkFactory = new MinioDataSinkFactory(minioConnectorAPI,
        monitor, executorService);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .build())
        .build();

    // When
    var result = minioDataSinkFactory.validateRequest(dataFlowStartMessage);

    // Then
    assertTrue(result.failed());
    assertEquals(1, result.getFailureMessages().size());
  }

  @Test
  void validateRequestShouldReturnFailureResultWhenDataFlowStartMessageDestinationDataAddressPropertyBucketNameIsEmpty() {
    // Given
    MinioDataSinkFactory minioDataSinkFactory = new MinioDataSinkFactory(minioConnectorAPI,
        monitor, executorService);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.BUCKET_NAME, "")
            .property(MinioBucketSchema.OBJECT_NAME, "objectName")
            .property(MinioBucketSchema.OBJECT_PREFIX, "objectPrefix")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .property(MinioBucketSchema.BUCKET_NAME, "")
            .property(MinioBucketSchema.OBJECT_NAME, "objectName")
            .property(MinioBucketSchema.OBJECT_PREFIX, "objectPrefix")
            .build())
        .build();

    // When
    var result = minioDataSinkFactory.validateRequest(dataFlowStartMessage);

    // Then
    assertTrue(result.failed());
    assertEquals(1, result.getFailureMessages().size());
  }

  @Test
  void validateRequestShouldReturnSuccessResultWhenDataFlowStartMessageDestinationDataAddressPropertyObjectNameIsEmpty() {
    // Given
    MinioDataSinkFactory minioDataSinkFactory = new MinioDataSinkFactory(minioConnectorAPI,
        monitor, executorService);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.BUCKET_NAME, "bucketName")
            .property(MinioBucketSchema.OBJECT_NAME, "")
            .property(MinioBucketSchema.OBJECT_PREFIX, "objectPrefix")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .property(MinioBucketSchema.BUCKET_NAME, "bucketName")
            .property(MinioBucketSchema.OBJECT_NAME, "")
            .property(MinioBucketSchema.OBJECT_PREFIX, "objectPrefix")
            .build())
        .build();

    // When
    var result = minioDataSinkFactory.validateRequest(dataFlowStartMessage);

    // Then
    assertTrue(result.succeeded());
    assertEquals(0, result.getFailureMessages().size());
  }

  @Test
  void validateRequestShouldReturnSuccessResultWhenDataFlowStartMessageDestinationDataAddressPropertyObjectPrefixIsEmpty() {
    // Given
    MinioDataSinkFactory minioDataSinkFactory = new MinioDataSinkFactory(minioConnectorAPI,
        monitor, executorService);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.BUCKET_NAME, "bucketName")
            .property(MinioBucketSchema.OBJECT_NAME, "objectName")
            .property(MinioBucketSchema.OBJECT_PREFIX, "")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .property(MinioBucketSchema.BUCKET_NAME, "bucketName")
            .property(MinioBucketSchema.OBJECT_NAME, "objectName")
            .property(MinioBucketSchema.OBJECT_PREFIX, "")
            .build())
        .build();

    // When
    var result = minioDataSinkFactory.validateRequest(dataFlowStartMessage);

    // Then
    assertTrue(result.succeeded());
    assertEquals(0, result.getFailureMessages().size());
  }

  @Test
  void createSinkShouldThrowExceptionWhenDestinationDataAddressIsInvalid() {
    // Given
    MinioDataSinkFactory minioDataSinkFactory = new MinioDataSinkFactory(minioConnectorAPI,
        monitor, executorService);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type("sourceType")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .build())
        .build();

    // When
    // Then
    assertThrows(EdcException.class,
        () -> minioDataSinkFactory.createSink(dataFlowStartMessage));
  }

    @Test
  void createSourceShouldReturnNonNullDataSinkWheDestinationDataAddressIsValid() {
    // Given
    MinioDataSinkFactory minioDataSinkFactory = new MinioDataSinkFactory(minioConnectorAPI,
        monitor, executorService);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.BUCKET_NAME, "bucketName")
            .property(MinioBucketSchema.OBJECT_NAME, "objectName")
            .property(MinioBucketSchema.OBJECT_PREFIX, "objectPrefix")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.BUCKET_NAME, "destinationBucketName")
            .property(MinioBucketSchema.OBJECT_NAME, "objectName")
            .build())
        .build();

    // When
    DataSink result = minioDataSinkFactory.createSink(dataFlowStartMessage);

    // Then
    assertNotNull(result);
  }

}
