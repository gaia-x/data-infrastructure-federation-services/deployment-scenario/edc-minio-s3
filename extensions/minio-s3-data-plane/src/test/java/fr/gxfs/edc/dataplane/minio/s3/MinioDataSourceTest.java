package fr.gxfs.edc.dataplane.minio.s3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import fr.gxfs.edc.extension.s3.api.MinioConnectorAPIImpl;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import org.eclipse.edc.connector.dataplane.spi.pipeline.StreamFailure.Reason;
import org.eclipse.edc.spi.EdcException;
import org.eclipse.edc.spi.monitor.Monitor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MinioDataSourceTest {

  public static final long MEGABYTES = 1024L * 1024L;
  private MinioConnectorAPIImpl minioConnectorAPI;
  private Monitor monitor;

  @BeforeEach
  void setUp() {
    monitor = mock(Monitor.class);
    minioConnectorAPI = mock(MinioConnectorAPIImpl.class);
  }


  @Test
  void builderShouldThrowNullPointerExceptionWhenMinioClientIsNull() {
    MinioDataSource.Builder builder = MinioDataSource.Builder.newInstance(null);

    assertThrows(NullPointerException.class, builder::build);
  }

  @Test
  void builderShouldThrowNullPointerExceptionWhenMonitorIsNull() {
    MinioDataSource.Builder builder = MinioDataSource.Builder.newInstance(minioConnectorAPI)
        .monitor(null);

    assertThrows(NullPointerException.class, builder::build);
  }

  @Test
  void builderShouldThrowNullPointerExceptionWhenBucketNameIsNull() {
    MinioDataSource.Builder builder = MinioDataSource.Builder.newInstance(minioConnectorAPI)
        .monitor(monitor)
        .bucketName(null);

    assertThrows(NullPointerException.class, builder::build);
  }

  @Test
  void builderShouldReturnMinioDataSourceWhenMandatoryDataAreSet() {
    MinioDataSource dataSource = MinioDataSource.Builder.newInstance(minioConnectorAPI)
        .monitor(monitor)
        .bucketName("bucketName")
        .objectName("objectName")
        .objectPrefix("objectPrefix")
        .build();

    assertNotNull(dataSource);
  }

  @Test
  void openPartShouldFailsIfNoObjectFoundWithGivenPrefix() {
    // Given
    when(minioConnectorAPI.listObjects("bucketName", "objectPrefix")).thenReturn(
        Collections.emptyList());

    MinioDataSource minioDataSource = MinioDataSource.Builder.newInstance(minioConnectorAPI)
        .monitor(monitor)
        .bucketName("bucketName")
        .objectPrefix("objectPrefix")
        .build();

    // When
    var result = minioDataSource.openPartStream();

    // Then
    assertTrue(result.failed());
    assertEquals(Reason.NOT_FOUND, result.reason());
  }

  @Test
  void openPartShouldSucceededIfObjectFoundWithGivenPrefix() {
    // Given
    when(minioConnectorAPI.listObjects("bucketName", "objectPrefix")).thenReturn(
        List.of("objectPrefix/object1.txt", "objectPrefix/object2.txt"));

    when(minioConnectorAPI.getObjectSize("bucketName", "objectPrefix/object1.txt")).thenReturn(
        MEGABYTES);
    when(minioConnectorAPI.getObjectSize("bucketName", "objectPrefix/object2.txt")).thenReturn(
        10 * MEGABYTES);

    MinioDataSource minioDataSource = MinioDataSource.Builder.newInstance(minioConnectorAPI)
        .monitor(monitor)
        .bucketName("bucketName")
        .objectPrefix("objectPrefix")
        .build();

    // When
    var result = minioDataSource.openPartStream();

    // Then
    assertTrue(result.succeeded());
    result.getContent().forEach(part -> assertTrue(part.name().startsWith("objectPrefix")));
  }

  @Test
  void openPartShouldFailsIfListObjectThrowEdcException() {
    // Given
    when(minioConnectorAPI.listObjects("bucketName", "objectName"))
        .thenThrow(EdcException.class);

    MinioDataSource minioDataSource = MinioDataSource.Builder.newInstance(minioConnectorAPI)
        .monitor(monitor)
        .bucketName("bucketName")
        .objectName("objectName")
        .build();

    // When
    var result = minioDataSource.openPartStream();

    // Then
    assertTrue(result.failed());
    assertEquals(Reason.GENERAL_ERROR, result.reason());
  }

  @Test
  void openPartShouldFailsIfNoObjectFound() {
    // Given
    when(minioConnectorAPI.listObjects("bucketName", "objectName")).thenReturn(
        Collections.emptyList());
    MinioDataSource minioDataSource = MinioDataSource.Builder.newInstance(minioConnectorAPI)
        .monitor(monitor)
        .bucketName("bucketName")
        .objectName("objectName")
        .build();

    // When
    var result = minioDataSource.openPartStream();

    // Then
    assertTrue(result.failed());
    assertEquals(Reason.NOT_FOUND, result.reason());
  }

  @Test
  void openPartShouldSucceededIfObjectFoundWithoutPrefix() {
    // Given
    when(minioConnectorAPI.listObjects("bucketName", "objectName")).thenReturn(
        List.of("objectName"));

    when(minioConnectorAPI.getObjectSize("bucketName", "objectName")).thenReturn(
        1024L * 1024L);

    MinioDataSource minioDataSource = MinioDataSource.Builder.newInstance(minioConnectorAPI)
        .monitor(monitor)
        .bucketName("bucketName")
        .objectName("objectName")
        .build();

    // When
    var result = minioDataSource.openPartStream();

    // Then
    assertTrue(result.succeeded());
    result.getContent().forEach(part -> assertEquals("objectName", part.name()));
  }

}

class MinioPartTest {

  private MinioConnectorAPIImpl minioConnectorAPI;
  private Monitor monitor;
  private MinioDataSource.MinioPart minioPart;

  @BeforeEach
  void setUp() {
    monitor = mock(Monitor.class);
    minioConnectorAPI = mock(MinioConnectorAPIImpl.class);
    minioPart = new MinioDataSource.MinioPart(minioConnectorAPI, "bucketName", "objectName", 0,
        1024L);

  }

  @Test
  void constructorShouldThrowNullPointerExceptionWhenMinioClientIsNull() {
    assertThrows(NullPointerException.class,
        () -> new MinioDataSource.MinioPart(null, "bucketName", "objectName", 0, 0));
  }

  @Test
  void constructorShouldThrowNullPointerExceptionWhenBucketNameIsNull() {
    assertThrows(NullPointerException.class,
        () -> new MinioDataSource.MinioPart(minioConnectorAPI, null, "objectName", 0, 0));
  }

  @Test
  void constructorShouldThrowNullPointerExceptionWhenObjectNameIsNull() {
    assertThrows(NullPointerException.class,
        () -> new MinioDataSource.MinioPart(minioConnectorAPI, "bucketName", null, 0, 0));
  }

  @Test
  void constructorShouldThrowIllegalArgumentExceptionWhenStartIsNegative() {
    assertThrows(IllegalArgumentException.class,
        () -> new MinioDataSource.MinioPart(minioConnectorAPI, "bucketName", "objectName", -1, 0));
  }

  @Test
  void constructorShouldThrowIllegalArgumentExceptionWhenEndIsNegative() {
    assertThrows(IllegalArgumentException.class,
        () -> new MinioDataSource.MinioPart(minioConnectorAPI, "bucketName", "objectName", 0, -1));
  }

  @Test
  void constructorShouldThrowIllegalArgumentExceptionWhenEndIsLessThanStart() {
    assertThrows(IllegalArgumentException.class,
        () -> new MinioDataSource.MinioPart(minioConnectorAPI, "bucketName", "objectName", 10, 5));
  }

  @Test
  void constructorShouldReturnMinioPartWhenAllDataAreValid() {
    MinioDataSource.MinioPart minioPart = new MinioDataSource.MinioPart(minioConnectorAPI,
        "bucketName", "objectName", 0, 10);

    assertNotNull(minioPart);
    assertEquals("objectName", minioPart.name());
  }

  @Test
  void openStreamShouldReturnStreamFailureWhenGetObjectThrowEdcException() {
    // Given
    when(minioConnectorAPI.getObject("bucketName", "objectName", 0, 1024L)).thenThrow(
        EdcException.class);

    // When
    var result = minioPart.openStream();

    // Then
    assertNull(result);
  }

  @Test
  void openStreamShouldReturnValidInputStreamIfObjectSizeIsLowerThanChunkSize() {
    // Given
    when(minioConnectorAPI.getObject("bucketName", "objectName"))
        .thenReturn(mock(ByteArrayInputStream.class));

    // When
    var result = minioPart.openStream();

    // Then
    assertNotNull(result);
    assertInstanceOf(ByteArrayInputStream.class, result);
    assertTrue(minioPart.isClosed());
  }

  @Test
  void openStreamShouldReturnValidInputStreamIfObjectSizeIsGreaterThanChunkSize() {
    // Given
    when(minioConnectorAPI.getObject("bucketName", "objectName"))
        .thenReturn(mock(ByteArrayInputStream.class));

    var minioPart2 = new MinioDataSource.MinioPart(minioConnectorAPI, "bucketName", "objectName", 0,
        1024L * 1024L * 10L);
    // When
    var result = minioPart2.openStream();

    // Then
    assertNotNull(result);
    assertInstanceOf(ByteArrayInputStream.class, result);
    assertTrue(minioPart2.isClosed());
  }

  @Test
  void openStreamWithOffsetAndSizeShouldReturnValidInputStreamWhenOffsetAndSizeAreOK() {
    // Given
    ByteArrayInputStream inputStream = mock(ByteArrayInputStream.class);
    when(inputStream.available()).thenReturn(1024);
    when(minioConnectorAPI.getObject(
        "bucketName", "objectName", 0, 1024L))
        .thenReturn(inputStream);

    // When
    var result = minioPart.openStream(0, 1024L);

    // Then
    assertNotNull(result);
    assertInstanceOf(ByteArrayInputStream.class, result);
    assertTrue(minioPart.isClosed());
  }

  @Test
  void openStreamWithOffsetAndSizeShouldReturnValidInputStreamWhenOffsetIsLowerThanSize() {
    // Given
    ByteArrayInputStream inputStream = mock(ByteArrayInputStream.class);
    when(inputStream.available()).thenReturn(1024);
    when(minioConnectorAPI.getObject(
        "bucketName", "objectName", 0, 1024L))
        .thenReturn(inputStream);

    // When
    minioPart = new MinioDataSource.MinioPart(minioConnectorAPI, "bucketName", "objectName", 0,
        2048L);
    var result = minioPart.openStream(0, 1024L);

    // Then
    assertNotNull(result);
    assertInstanceOf(ByteArrayInputStream.class, result);
    assertFalse(minioPart.isClosed());
  }

  @Test
  void openStreamWithOffsetAndSizeShouldThrowsIllegalStateExceptionWhenPartIsClosed() {
    // Given
    ByteArrayInputStream inputStream = mock(ByteArrayInputStream.class);
    when(inputStream.available()).thenReturn(1024);
    when(minioConnectorAPI.getObject(
        "bucketName", "objectName", 0, 1024L))
        .thenReturn(inputStream);

    // When
    minioPart.openStream(0, 1024L);

    // Then
    assertThrows(IllegalStateException.class, () -> minioPart.openStream(0, 1024L));
  }

  @Test
  void openStreamWithOffsetAndSizeShouldThrowsEdcExceptionWhenAccessingInputStream() {
    // Given
    ByteArrayInputStream inputStream = mock(ByteArrayInputStream.class);
    when(inputStream.available()).thenThrow(IOException.class);
    when(minioConnectorAPI.getObject(
        "bucketName", "objectName", 0, 1024L))
        .thenReturn(inputStream);

    // When
    // Then
    assertThrows(EdcException.class, () -> minioPart.openStream(0, 1024L));
  }

}