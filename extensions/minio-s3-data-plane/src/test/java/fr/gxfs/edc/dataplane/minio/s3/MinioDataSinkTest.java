package fr.gxfs.edc.dataplane.minio.s3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import fr.gxfs.edc.extension.s3.api.MinioConnectorAPIImpl;
import java.util.concurrent.ExecutorService;
import org.eclipse.edc.spi.monitor.Monitor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MinioDataSinkTest {

  private MinioConnectorAPIImpl minioConnectorAPI;
  private Monitor monitor;
  private ExecutorService executorService;
  private MinioDataSource.MinioPart part;

  @BeforeEach
  void setUp() {
    monitor = mock(Monitor.class);
    minioConnectorAPI = mock(MinioConnectorAPIImpl.class);
    executorService = mock(ExecutorService.class);

    part = mock(MinioDataSource.MinioPart.class);
    when(part.name()).thenReturn("partName");
  }

  @Test
  void builderShouldThrowNullPointerExceptionWhenMinioConnectorClientIsNull() {
    MinioDataSink.Builder builder = MinioDataSink.Builder.newInstance()
        .monitor(monitor)
        .requestId("requestId")
        .bucketName("bucketName")
        .executorService(executorService)

        .bucketName(null);

    assertThrows(NullPointerException.class, builder::build);
  }

  @Test
  void builderShouldThrowNullPointerExceptionWhenBucketNameIsNull() {
    MinioDataSink.Builder builder = MinioDataSink.Builder.newInstance()
        .monitor(monitor)
        .minioConnectorAPI(minioConnectorAPI)
        .requestId("requestId")
        .executorService(executorService)

        .bucketName(null);

    assertThrows(NullPointerException.class, builder::build);
  }

  @Test
  void builderShouldReturnMinioDataSinkWhenMandatoryDataAreSet() {
    MinioDataSink dataSink = MinioDataSink.Builder.newInstance()
        .monitor(monitor)
        .requestId("requestId")
        .executorService(executorService)
        .minioConnectorAPI(minioConnectorAPI)
        .bucketName("bucketName")
        .objectName("objectName")
        .build();

    assertNotNull(dataSink);
  }

  @Test
  void determineDestinationNameShouldReturnNonEmptyStringWhenPathAndObjectNameNonNull() {
    MinioDataSink dataSink = MinioDataSink.Builder.newInstance()
        .monitor(monitor)
        .requestId("requestId")
        .executorService(executorService)
        .minioConnectorAPI(minioConnectorAPI)
        .bucketName("bucketName")
        .objectName("objectName")
        .path("path/")
        .build();

    String destination = dataSink.determineDestinationName(mock(MinioDataSource.MinioPart.class));

    assertNotNull(destination);
    assertNotEquals("", destination);
    assertEquals("path/objectName", destination);
  }

  @Test
  void determineDestinationShouldReturnNonEmptyStringWhenPathNullAndObjectNameNonNull() {
    MinioDataSink dataSink = MinioDataSink.Builder.newInstance()
        .monitor(monitor)
        .requestId("requestId")
        .executorService(executorService)
        .minioConnectorAPI(minioConnectorAPI)
        .bucketName("bucketName")
        .objectName("objectName")
        .build();

    String destination = dataSink.determineDestinationName(mock(MinioDataSource.MinioPart.class));

    assertNotNull(destination);
    assertNotEquals("", destination);
    assertEquals("objectName", destination);
  }

  @Test
  void determineDestinationShouldReturnNonEmptyStringWhenPathNonNullAndObjectNameNull() {
    MinioDataSink dataSink = MinioDataSink.Builder.newInstance()
        .monitor(monitor)
        .requestId("requestId")
        .executorService(executorService)
        .minioConnectorAPI(minioConnectorAPI)
        .bucketName("bucketName")
        .path("path/")
        .build();

    var part = mock(MinioDataSource.MinioPart.class);
    when(part.name()).thenReturn("partName");

    String destination = dataSink.determineDestinationName(part);

    assertNotNull(destination);
    assertNotEquals("", destination);
    assertEquals("path/partName", destination);
  }

  @Test
  void determineDestinationShouldReturnNonEmptyStringWhenPathNullAndObjectNameNull() {
    MinioDataSink dataSink = MinioDataSink.Builder.newInstance()
        .monitor(monitor)
        .requestId("requestId")
        .executorService(executorService)
        .minioConnectorAPI(minioConnectorAPI)
        .bucketName("bucketName")
        .build();

    var part = mock(MinioDataSource.MinioPart.class);
    when(part.name()).thenReturn("partName");

    String destination = dataSink.determineDestinationName(part);

    assertNotNull(destination);
    assertNotEquals("", destination);
    assertEquals("partName", destination);
  }

  @Test
  void determineDestinationShouldReturnPartNameWhenPathAndObjectNameAreNull() {
    MinioDataSink dataSink = MinioDataSink.Builder.newInstance()
        .monitor(monitor)
        .requestId("requestId")
        .executorService(executorService)
        .minioConnectorAPI(minioConnectorAPI)
        .bucketName("bucketName")
        .build();

    String destination = dataSink.determineDestinationName(part);
    assertEquals("partName", destination);
  }

  @Test
  void determineDestinationShouldReturnPartNameWhenPathAndObjectNameAreEmpty() {
    MinioDataSink dataSink = MinioDataSink.Builder.newInstance()
        .monitor(monitor)
        .requestId("requestId")
        .executorService(executorService)
        .minioConnectorAPI(minioConnectorAPI)
        .bucketName("bucketName")
        .objectName("")
        .path("")
        .build();
    String destination = dataSink.determineDestinationName(part);
    assertEquals("", destination);
  }

  @Test
  void determineDestinationNameShouldHandleSpecialCharactersInPathAndObjectName() {
    MinioDataSink dataSink = MinioDataSink.Builder.newInstance()
        .monitor(monitor)
        .requestId("requestId")
        .executorService(executorService)
        .minioConnectorAPI(minioConnectorAPI)
        .bucketName("bucketName")
        .path("path/with/special/chars#%$&")
        .objectName("objectName#%$&")
        .build();

    String destination = dataSink.determineDestinationName(part);
    assertEquals("path/with/special/chars#%$&objectName#%$&", destination);
  }

  @Test
  void determineDestinationNameShouldHandleSpecialCharactersInPartName() {
    MinioDataSink dataSink = MinioDataSink.Builder.newInstance()
        .monitor(monitor)
        .requestId("requestId")
        .executorService(executorService)
        .minioConnectorAPI(minioConnectorAPI)
        .bucketName("bucketName")
        .build();
    when(part.name()).thenReturn("partName#%$&");
    String destination = dataSink.determineDestinationName(part);
    assertEquals("partName#%$&", destination);
  }
}
