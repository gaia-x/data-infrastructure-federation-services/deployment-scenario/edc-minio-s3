package fr.gxfs.edc.dataplane.minio.s3;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import fr.gxfs.edc.extension.s3.api.MinioConnectorAPIImpl;
import fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema;
import org.eclipse.edc.connector.dataplane.spi.pipeline.DataSource;
import org.eclipse.edc.spi.EdcException;
import org.eclipse.edc.spi.monitor.Monitor;
import org.eclipse.edc.spi.types.domain.DataAddress;
import org.eclipse.edc.spi.types.domain.transfer.DataFlowStartMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Collections;
import java.util.List;

class MinioDataSourceFactoryTest {

  private MinioConnectorAPIImpl minioConnectorAPI;
  private Monitor monitor;

  @BeforeEach
  void setUp() {
    monitor = mock(Monitor.class);
    minioConnectorAPI = mock(MinioConnectorAPIImpl.class);
  }

  @Test
  void constructorShouldThrowNullPointerExceptionWhenMinioClientIsNull() {
    assertThrows(NullPointerException.class, () -> new MinioDataSourceFactory(null, monitor));
  }

  @Test
  void constructorShouldThrowNullPointerExceptionWhenMonitorIsNull() {
    assertThrows(NullPointerException.class, () -> new MinioDataSourceFactory(minioConnectorAPI, null));
  }

  @Test
  void constructorShouldNotThrowExceptionWhenMinioClientAndMonitorAreNotNull() {
    assertDoesNotThrow(() -> new MinioDataSourceFactory(minioConnectorAPI, monitor));
  }

  @Test
  void canHandleShouldReturnTrueWhenDataFlowStartMessageSourceDataAddressTypeIsMinioBucketSchemaType() {
    // Given
    MinioDataSourceFactory minioDataSourceFactory = new MinioDataSourceFactory(minioConnectorAPI, monitor);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .build())
        .build();

    // When
    boolean result = minioDataSourceFactory.canHandle(dataFlowStartMessage);

    // Then
    assertTrue(result);
  }

  @Test
  void canHandleShouldReturnFalseWhenDataFlowStartMessageSourceDataAddressTypeIsNotMinioBucketSchemaType() {
    // Given
    MinioDataSourceFactory minioDataSourceFactory = new MinioDataSourceFactory(minioConnectorAPI, monitor);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type("sourceType")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .build())
        .build();

    // When
    boolean result = minioDataSourceFactory.canHandle(dataFlowStartMessage);

    // Then
    assertFalse(result);
  }

  @Test
  void validateRequestShouldReturnSuccessResultWhenDataFlowStartMessageSourceDataAddressIsValid() {
    // Given
    MinioDataSourceFactory minioDataSourceFactory = new MinioDataSourceFactory(minioConnectorAPI, monitor);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.BUCKET_NAME, "bucketName")
            .property(MinioBucketSchema.OBJECT_PREFIX, "objectPrefix")
            .property(MinioBucketSchema.OBJECT_NAME, "objectName")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .build())
        .build();

    // When
    var result = minioDataSourceFactory.validateRequest(dataFlowStartMessage);

    // Then
    assertTrue(result.succeeded());
  }

  @Test
  void validateRequestShouldReturnFailureResultWhenDataFlowStartMessageSourceDataAddressPropertyBucketNameIsMissing() {
    // Given
    MinioDataSourceFactory minioDataSourceFactory = new MinioDataSourceFactory(minioConnectorAPI, monitor);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.OBJECT_NAME, "objectName")
            .property(MinioBucketSchema.OBJECT_PREFIX, "objectPrefix")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .build())
        .build();

    // When
    var result = minioDataSourceFactory.validateRequest(dataFlowStartMessage);

    // Then
    assertTrue(result.failed());
    assertEquals(1, result.getFailureMessages().size());
  }

  @Test
  void validateRequestShouldReturnFailureResultWhenDataFlowStartMessageSourceDataAddressPropertyObjectNameIsMissing() {
    // Given
    MinioDataSourceFactory minioDataSourceFactory = new MinioDataSourceFactory(minioConnectorAPI, monitor);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.BUCKET_NAME, "bucketName")
            .property(MinioBucketSchema.OBJECT_PREFIX, "objectPrefix")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .build())
        .build();

    // When
    var result = minioDataSourceFactory.validateRequest(dataFlowStartMessage);

    // Then
    assertTrue(result.failed());
    assertEquals(1, result.getFailureMessages().size());
  }

  @Test
  void validateRequestShouldReturnFailureResultWhenDataFlowStartMessageSourceDataAddressPropertyObjectPrefixIsMissing() {
    // Given
    MinioDataSourceFactory minioDataSourceFactory = new MinioDataSourceFactory(minioConnectorAPI, monitor);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.BUCKET_NAME, "bucketName")
            .property(MinioBucketSchema.OBJECT_NAME, "objectName")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .build())
        .build();

    // When
    var result = minioDataSourceFactory.validateRequest(dataFlowStartMessage);

    // Then
    assertTrue(result.succeeded());
    assertEquals(0, result.getFailureMessages().size());
  }

  @Test
  void validateRequestShouldReturnFailureResultWhenDataFlowStartMessageSourceDataAddressIsInvalid() {
    // Given
    MinioDataSourceFactory minioDataSourceFactory = new MinioDataSourceFactory(minioConnectorAPI, monitor);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .build())
        .build();

    // When
    var result = minioDataSourceFactory.validateRequest(dataFlowStartMessage);

    // Then
    assertTrue(result.failed());
    assertEquals(2, result.getFailureMessages().size());
  }

  @Test
  void validateRequestShouldReturnFailureResultWhenDataFlowStartMessageSourceDataAddressPropertyBucketNameIsEmpty() {
    // Given
    MinioDataSourceFactory minioDataSourceFactory = new MinioDataSourceFactory(minioConnectorAPI, monitor);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.BUCKET_NAME, "")
            .property(MinioBucketSchema.OBJECT_NAME, "objectName")
            .property(MinioBucketSchema.OBJECT_PREFIX, "objectPrefix")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .build())
        .build();

    // When
    var result = minioDataSourceFactory.validateRequest(dataFlowStartMessage);

    // Then
    assertTrue(result.failed());
    assertEquals(1, result.getFailureMessages().size());
  }

  @Test
  void validateRequestShouldReturnFailureResultWhenDataFlowStartMessageSourceDataAddressPropertyObjectNameIsEmpty() {
    // Given
    MinioDataSourceFactory minioDataSourceFactory = new MinioDataSourceFactory(minioConnectorAPI, monitor);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.BUCKET_NAME, "bucketName")
            .property(MinioBucketSchema.OBJECT_NAME, "")
            .property(MinioBucketSchema.OBJECT_PREFIX, "objectPrefix")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .build())
        .build();

    // When
    var result = minioDataSourceFactory.validateRequest(dataFlowStartMessage);

    // Then
    assertTrue(result.failed());
    assertEquals(1, result.getFailureMessages().size());
  }

  @Test
  void validateRequestShouldReturnSuccessResultWhenDataFlowStartMessageSourceDataAddressPropertyObjectPrefixIsEmpty() {
    // Given
    MinioDataSourceFactory minioDataSourceFactory = new MinioDataSourceFactory(minioConnectorAPI, monitor);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.BUCKET_NAME, "bucketName")
            .property(MinioBucketSchema.OBJECT_NAME, "objectName")
            .property(MinioBucketSchema.OBJECT_PREFIX, "")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .build())
        .build();

    // When
    var result = minioDataSourceFactory.validateRequest(dataFlowStartMessage);

    // Then
    assertTrue(result.succeeded());
    assertEquals(0, result.getFailureMessages().size());
  }

  @Test
  void createSourceShouldThrowExceptionWhenSourceDataAddressIsInvalid() {
    // Given
    MinioDataSourceFactory minioDataSourceFactory = new MinioDataSourceFactory(minioConnectorAPI, monitor);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type("sourceType")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .build())
        .build();

    // When
    // Then
    assertThrows(EdcException.class,
        () -> minioDataSourceFactory.createSource(dataFlowStartMessage));
  }

  @Test
  void createSourceShouldReturnNonNullDataSourceWhenSourceDataAddressIsValid() {
    // Given
    MinioDataSourceFactory minioDataSourceFactory = new MinioDataSourceFactory(minioConnectorAPI, monitor);

    DataFlowStartMessage dataFlowStartMessage = DataFlowStartMessage.Builder.newInstance()
        .processId("processId")
        .sourceDataAddress(DataAddress.Builder.newInstance()
            .type(MinioBucketSchema.TYPE)
            .property(MinioBucketSchema.BUCKET_NAME, "bucketName")
            .property(MinioBucketSchema.OBJECT_NAME, "objectName")
            .property(MinioBucketSchema.OBJECT_PREFIX, "objectPrefix")
            .build())
        .destinationDataAddress(DataAddress.Builder.newInstance()
            .type("destinationType")
            .build())
        .build();

    // When
    DataSource result = minioDataSourceFactory.createSource(dataFlowStartMessage);

    // Then
    assertNotNull(result);
  }

}
