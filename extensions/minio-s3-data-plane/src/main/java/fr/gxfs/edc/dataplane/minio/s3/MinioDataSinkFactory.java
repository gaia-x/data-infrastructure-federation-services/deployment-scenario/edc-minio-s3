package fr.gxfs.edc.dataplane.minio.s3;

import fr.gxfs.edc.extension.s3.api.MinioConnectorAPI;
import fr.gxfs.edc.extension.s3.api.MinioConnectorAPIImpl;
import fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema;
import fr.gxfs.edc.extension.s3.validation.MinioDestinationDataAddressValidator;
import org.eclipse.edc.connector.dataplane.spi.pipeline.DataSink;
import org.eclipse.edc.connector.dataplane.spi.pipeline.DataSinkFactory;
import org.eclipse.edc.spi.EdcException;
import org.eclipse.edc.spi.monitor.Monitor;
import org.eclipse.edc.spi.result.Result;
import org.eclipse.edc.spi.types.domain.transfer.DataFlowStartMessage;
import org.eclipse.edc.validator.spi.ValidationResult;
import org.jetbrains.annotations.NotNull;
import java.util.concurrent.ExecutorService;

/**
 * A {@link DataSinkFactory} implementation for Minio S3 buckets.
 */
public class MinioDataSinkFactory implements DataSinkFactory {

  private final MinioConnectorAPI minioConnectorAPI;
  private final Monitor monitor;
  private final MinioDestinationDataAddressValidator validator = new MinioDestinationDataAddressValidator();
  private final ExecutorService executorService;

  public MinioDataSinkFactory(MinioConnectorAPI minioConnectorAPI, Monitor monitor, ExecutorService executorService) {
    this.minioConnectorAPI = minioConnectorAPI;
    this.monitor = monitor;
    this.executorService = executorService;
  }

  /**
   * Determines whether this factory can handle a {@link DataFlowStartMessage}.
   *
   * @param dataFlowStartMessage the {@link DataFlowStartMessage} to check.
   * @return {@code true} if this factory can handle the {@link DataFlowStartMessage}.
   */
  @Override
  public boolean canHandle(DataFlowStartMessage dataFlowStartMessage) {
    return MinioBucketSchema.TYPE.equals(
        dataFlowStartMessage.getDestinationDataAddress().getType());
  }

  /**
   * @param dataFlowStartMessage
   * @return
   */
  @Override
  public DataSink createSink(DataFlowStartMessage dataFlowStartMessage) {
    var validationResult = validateRequest(dataFlowStartMessage);
    if (validationResult.failed()) {
      throw new EdcException(String.join(", ", validationResult.getFailureMessages()));
    }

    var destination = dataFlowStartMessage.getDestinationDataAddress();
    var builder = MinioDataSink.Builder.newInstance()
        .requestId(dataFlowStartMessage.getId())
        .monitor(monitor)
        .minioConnectorAPI(minioConnectorAPI)
        .executorService(executorService)
        .bucketName(destination.getStringProperty(MinioBucketSchema.BUCKET_NAME));

    if (destination.hasProperty(MinioBucketSchema.PATH)) {
      builder.path(destination.getStringProperty(MinioBucketSchema.PATH));
    }

    if (destination.hasProperty(MinioBucketSchema.OBJECT_NAME)) {
      builder.objectName(destination.getStringProperty(MinioBucketSchema.OBJECT_NAME));
    }

    return builder
        .build();
  }

  /**
   * Validates the request.
   *
   * @param dataFlowStartMessage the {@link DataFlowStartMessage} to validate.
   * @return a {@link Result} indicating whether the request is valid.
   */
  @Override
  public @NotNull Result<Void> validateRequest(DataFlowStartMessage dataFlowStartMessage) {
    return validator.validate(dataFlowStartMessage.getDestinationDataAddress()).flatMap(
        ValidationResult::toResult);
  }
}
