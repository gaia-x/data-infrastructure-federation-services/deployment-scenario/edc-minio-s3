package fr.gxfs.edc.dataplane.minio;

import fr.gxfs.edc.dataplane.minio.s3.MinioDataSinkFactory;
import fr.gxfs.edc.dataplane.minio.s3.MinioDataSourceFactory;
import fr.gxfs.edc.extension.s3.api.MinioConnectorAPI;
import org.eclipse.edc.connector.dataplane.spi.pipeline.DataTransferExecutorServiceContainer;
import org.eclipse.edc.connector.dataplane.spi.pipeline.PipelineService;
import org.eclipse.edc.runtime.metamodel.annotation.Extension;
import org.eclipse.edc.runtime.metamodel.annotation.Inject;
import org.eclipse.edc.spi.monitor.Monitor;
import org.eclipse.edc.spi.security.Vault;
import org.eclipse.edc.spi.system.ServiceExtension;
import org.eclipse.edc.spi.system.ServiceExtensionContext;
import org.eclipse.edc.spi.types.TypeManager;
import java.util.concurrent.Executors;

@Extension(value = MinioDataPlaneExtension.NAME)
public class MinioDataPlaneExtension implements ServiceExtension {

  public static final String NAME = "Data plane - Minio";

  @Inject
  private PipelineService pipelineService;

  @Inject
  private MinioConnectorAPI minioAPI;

  @Inject
  private DataTransferExecutorServiceContainer executorContainer;

  @Inject
  private Vault vault;

  @Inject
  private TypeManager typeManager;

  @Inject
  private Monitor monitor;

  @Override
  public String name() {
    return NAME;
  }

  @Override
  public void initialize(ServiceExtensionContext context) {
    monitor.debug("Initializing Minio Provision extension");

    var sourceFactory = new MinioDataSourceFactory(minioAPI, monitor);
    pipelineService.registerFactory(sourceFactory);

    var sinkFactory = new MinioDataSinkFactory(minioAPI, monitor, Executors.newFixedThreadPool(10));
    pipelineService.registerFactory(sinkFactory);

    monitor.debug("Minio Provision extension initialized");
  }
}
