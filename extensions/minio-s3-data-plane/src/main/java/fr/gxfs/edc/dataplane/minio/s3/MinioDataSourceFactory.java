package fr.gxfs.edc.dataplane.minio.s3;


import fr.gxfs.edc.extension.s3.api.MinioConnectorAPI;
import fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema;
import fr.gxfs.edc.extension.s3.validation.MinioSourceDataAddressValidator;
import org.eclipse.edc.connector.dataplane.spi.pipeline.DataSource;
import org.eclipse.edc.connector.dataplane.spi.pipeline.DataSourceFactory;
import org.eclipse.edc.spi.EdcException;
import org.eclipse.edc.spi.monitor.Monitor;
import org.eclipse.edc.spi.result.Result;
import org.eclipse.edc.spi.types.domain.transfer.DataFlowStartMessage;
import org.eclipse.edc.validator.spi.ValidationResult;
import org.jetbrains.annotations.NotNull;
import java.util.Objects;

/**
 * A {@link DataSourceFactory} implementation for Minio S3 buckets.
 */
public class MinioDataSourceFactory implements DataSourceFactory {

  private final MinioConnectorAPI minioClient;
  private final MinioSourceDataAddressValidator validator = new MinioSourceDataAddressValidator();
  private final Monitor monitor;

  public MinioDataSourceFactory(MinioConnectorAPI minioClient, Monitor monitor) {
    Objects.requireNonNull(minioClient, "minioClient must not be null");
    Objects.requireNonNull(monitor, "monitor must not be null");
    this.minioClient = minioClient;
    this.monitor = monitor;
  }

  /**
   * Determines whether this factory can handle a {@link DataFlowStartMessage}.
   *
   * @param dataFlowStartMessage the {@link DataFlowStartMessage} to check.
   * @return {@code true} if this factory can handle the {@link DataFlowStartMessage}.
   */
  @Override
  public boolean canHandle(@NotNull DataFlowStartMessage dataFlowStartMessage) {
    monitor.debug("Checking if MinioDataSourceFactory can handle the DataFlowStartMessage.");
    monitor.debug("DataFlowStartMessage source data address type: "
        + dataFlowStartMessage.getSourceDataAddress().getType());
    monitor.debug("MinioDataSourceFactory can handle: " + MinioBucketSchema.TYPE);

    return MinioBucketSchema.TYPE.equals(dataFlowStartMessage.getSourceDataAddress().getType());
  }

  /**
   * Creates a new {@link DataSource} for a {@link DataFlowStartMessage}.
   *
   * @param dataFlowStartMessage the {@link DataFlowStartMessage} to create a {@link DataSource}
   *                             for.
   * @return the new {@link DataSource}.
   */
  @Override
  public DataSource createSource(DataFlowStartMessage dataFlowStartMessage) {
    monitor.debug("Creating a new MinioDataSource for the DataFlowStartMessage.");
    monitor.debug("DataFlowStartMessage source data address: "
        + dataFlowStartMessage.getSourceDataAddress());
    monitor.debug("DataFlowStartMessage source data address properties: "
        + dataFlowStartMessage.getSourceDataAddress().getProperties());

    var validationResult = validateRequest(dataFlowStartMessage);

    if (validationResult.failed()) {
      monitor.severe("Validation failed: " + validationResult.getFailureMessages());
      throw new EdcException(String.join("; ", validationResult.getFailureMessages()));
    }

    var source = dataFlowStartMessage.getSourceDataAddress();

    return MinioDataSource.Builder.newInstance(this.minioClient)
        .monitor(this.monitor)
        .bucketName(source.getStringProperty(MinioBucketSchema.BUCKET_NAME))
        .objectName(source.getStringProperty(MinioBucketSchema.OBJECT_NAME))
        .objectPrefix(source.getStringProperty(MinioBucketSchema.OBJECT_PREFIX))
        .build();
  }

  /**
   * Validates a {@link DataFlowStartMessage}.
   *
   * @param dataFlowStartMessage the {@link DataFlowStartMessage} to validate.
   * @return the validation result.
   */
  @Override
  public @NotNull Result<Void> validateRequest(DataFlowStartMessage dataFlowStartMessage) {
    return validator.validate(dataFlowStartMessage.getSourceDataAddress()).flatMap(
        ValidationResult::toResult);
  }
}
