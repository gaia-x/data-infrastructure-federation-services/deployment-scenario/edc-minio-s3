package fr.gxfs.edc.dataplane.minio.s3;

import java.util.List;
import java.util.Objects;
import fr.gxfs.edc.extension.s3.api.MinioConnectorAPI;
import org.eclipse.edc.connector.dataplane.spi.pipeline.DataSource.Part;
import org.eclipse.edc.connector.dataplane.spi.pipeline.StreamResult;
import org.eclipse.edc.connector.dataplane.util.sink.ParallelSink;
import org.jetbrains.annotations.NotNull;

public class MinioDataSink extends ParallelSink {

  private MinioDataSink() {
  }

  private MinioConnectorAPI minioConnectorAPI;
  private String bucketName;
  private String objectName;
  private String path;

  @Override
  protected StreamResult<Object> transferParts(List<Part> parts) {
    for (Part part : parts) {
      String destination = determineDestinationName(part);
      try {
        uploadPartToMinio(part, destination);
      } catch (Exception e) {
        return handleUploadException(part, e);
      }
    }

    return StreamResult.success();
  }

  private @NotNull StreamResult<Object> handleUploadException(Part part, Exception e) {
    var message = String.format("Error uploading part %s to Minio bucket %s", part.name(),
        bucketName);
    monitor.severe(message, e);
    return StreamResult.error(message);
  }

  private void uploadPartToMinio(Part part, String destination) {
    minioConnectorAPI.putObject(bucketName, destination, part.openStream());
  }

  String determineDestinationName(Part part) {
    String destination = "";

    if (path != null && objectName != null) {
      destination = path + objectName;
    } else if (path == null && objectName != null) {
      destination = objectName;
    } else if (path != null) {
      destination = path + part.name();
    } else {
      destination = part.name();
    }

    return destination;
  }

  public static class Builder extends ParallelSink.Builder<Builder, MinioDataSink> {

    private Builder() {
      super(new MinioDataSink());
    }

    public static Builder newInstance() {
      return new Builder();
    }

    public Builder minioConnectorAPI(MinioConnectorAPI minioConnectorAPI) {
      sink.minioConnectorAPI = minioConnectorAPI;
      return this;
    }

    public Builder bucketName(String bucketName) {
      sink.bucketName = bucketName;
      return this;
    }

    public Builder objectName(String objectName) {
      sink.objectName = objectName;
      return this;
    }

    public Builder path(String path) {
      sink.path = path;
      return this;
    }

    @Override
    protected void validate() {
      Objects.requireNonNull(sink.minioConnectorAPI, "minio client is required");
      Objects.requireNonNull(sink.bucketName, "Bucket Name is required");
    }
  }
}
