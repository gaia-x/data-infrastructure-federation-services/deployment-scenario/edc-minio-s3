package fr.gxfs.edc.extension.s3.settings;

import org.eclipse.edc.runtime.metamodel.annotation.Setting;

/**
 * The schema for the Minio settings.
 */
public final class MinioSettingsSchema {

  /**
   * The key of the secret where the Minio Access Key Id is stored.
   */
  @Setting(value = "The key of the secret where the Minio Access Key Id is stored")
  public static final String MINIO_S3_ACCESS_KEY = "fr.gxfs.s3.access.key";

  /**
   * The key of the secret where the Minio Secret Key Id is stored.
   */
  @Setting(value = "The key of the secret where the Minio Secret Key Id is stored")
  public static final String MINIO_S3_SECRET_KEY = "fr.gxfs.s3.secret.key";

  /**
   * The key of the secret where the Minio Endpoint is stored.
   */
  @Setting(value = "The key of the secret where the Minio Endpoint is stored")
  public static final String MINIO_S3_ENDPOINT = "fr.gxfs.s3.endpoint";

  private MinioSettingsSchema() {
  }
}
