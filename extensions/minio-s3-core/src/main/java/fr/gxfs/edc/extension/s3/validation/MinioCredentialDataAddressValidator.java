package fr.gxfs.edc.extension.s3.validation;

import static fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema.ACCESS_KEY_ID;
import static fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema.ENDPOINT;
import static fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema.SECRET_ACCESS_KEY;

import org.eclipse.edc.spi.types.domain.DataAddress;
import org.eclipse.edc.validator.spi.ValidationResult;
import org.eclipse.edc.validator.spi.Validator;

/**
 * Validator for S3 data addresses containing Minio S3 credentials. This validator checks that the
 * required fields (access_key_id, secret_access_key and endpoint) are not null or empty.
 */
public class MinioCredentialDataAddressValidator extends MinioDataAddressValidator implements
    Validator<DataAddress> {

  /**
   * Validate the given data address. The fields to validate are the access_key_id,
   * secret_access_key and endpoint.
   *
   * @param dataAddress the data address to validate
   * @return the validation result. If the validation is successful, the result is a success. If the
   * validation fails, the result is a failure with the list of violations.
   */
  @Override
  public ValidationResult validate(DataAddress dataAddress) {
    return validate(dataAddress, ACCESS_KEY_ID, SECRET_ACCESS_KEY, ENDPOINT);
  }
}