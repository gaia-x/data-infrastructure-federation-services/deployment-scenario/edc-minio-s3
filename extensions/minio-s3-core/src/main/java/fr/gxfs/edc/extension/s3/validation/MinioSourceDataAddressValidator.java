package fr.gxfs.edc.extension.s3.validation;

import static fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema.BUCKET_NAME;
import static fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema.OBJECT_NAME;
import static fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema.OBJECT_PREFIX;

import org.eclipse.edc.spi.types.domain.DataAddress;
import org.eclipse.edc.validator.spi.ValidationResult;
import org.eclipse.edc.validator.spi.Validator;

/**
 * Validator for S3 data addresses containing Minio S3 source data. This validator checks that the
 * required fields (bucket_name, region, object_name and object_prefix) are not null or empty.
 */
public class MinioSourceDataAddressValidator extends MinioDataAddressValidator implements
        Validator<DataAddress> {

    /**
     * Validate the given data address. The fields to validate are the bucket_name, region, object_name and object_prefix. If one
     * of these fields is invalid, a violation is returned and the validation fails.
     *
     * @param dataAddress the data address to validate
     * @return the validation result. If the validation is successful, the result is a success. If the validation fails,
     * the result is a failure with the list of violations.
     */
    @Override
    public ValidationResult validate(DataAddress dataAddress) {
      return validate(dataAddress, BUCKET_NAME, OBJECT_NAME);
    }
}