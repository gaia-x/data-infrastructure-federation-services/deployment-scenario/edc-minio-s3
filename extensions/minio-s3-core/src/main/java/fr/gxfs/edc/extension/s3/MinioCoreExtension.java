package fr.gxfs.edc.extension.s3;

import static fr.gxfs.edc.extension.s3.settings.MinioSettingsSchema.MINIO_S3_ACCESS_KEY;
import static fr.gxfs.edc.extension.s3.settings.MinioSettingsSchema.MINIO_S3_ENDPOINT;
import static fr.gxfs.edc.extension.s3.settings.MinioSettingsSchema.MINIO_S3_SECRET_KEY;

import fr.gxfs.edc.extension.s3.api.MinioConnectorAPI;
import fr.gxfs.edc.extension.s3.api.MinioConnectorAPIImpl;
import fr.gxfs.edc.extension.s3.utils.MinioClientBuilderImpl;
import org.eclipse.edc.runtime.metamodel.annotation.Extension;
import org.eclipse.edc.runtime.metamodel.annotation.Inject;
import org.eclipse.edc.runtime.metamodel.annotation.Provides;
import org.eclipse.edc.spi.monitor.Monitor;
import org.eclipse.edc.spi.system.ServiceExtension;
import org.eclipse.edc.spi.system.ServiceExtensionContext;

/**
 * This class provides an implementation of the ServiceExtension interface to handle connection to
 * Minio S3.
 */
@Provides(value = MinioConnectorAPI.class)
@Extension(value = MinioCoreExtension.NAME)
public class MinioCoreExtension implements ServiceExtension {

  /**
   * The name of this extension. It is used to identify the extension in the EDC system.
   */
  public static final String NAME = "MinIO_S3";

  /**
   * The monitor used for logging. It is automatically injected by the runtime.
   */
  @Inject
  private Monitor monitor;

  /**
   * Returns the name of this extension.
   *
   * @return the name of this extension
   */
  @Override
  public String name() {
    return NAME;
  }

  /**
   * Initializes the extension. This method is responsible for retrieving the S3 credentials from
   * the configuration file, creating a MinioClient with these credentials, and registering the
   * S3ConnectorAPI service.
   *
   * @param context the ServiceExtensionContext
   */
  @Override
  public void initialize(ServiceExtensionContext context) {
    monitor.info("Initializing MinioCoreExtension");
    monitor.debug("Getting Minio Credentials from configuration file");

    // Get S3 credentials from configuration file
    String accessKey = context.getSetting(MINIO_S3_ACCESS_KEY, "");
    String secretKey = context.getSetting(MINIO_S3_SECRET_KEY, "");
    String endpoint = context.getSetting(MINIO_S3_ENDPOINT, "");

    monitor.debug("Current Access Key: " + accessKey);
    monitor.debug("Current Secret Key: " + secretKey);

    var minioClient = MinioClientBuilderImpl.builder()
        .credentials(accessKey, secretKey)
        .endpoint(endpoint)
        .build();

    context.registerService(MinioConnectorAPI.class,
        new MinioConnectorAPIImpl(minioClient, monitor));
  }
}