package fr.gxfs.edc.extension.s3.validator;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema;
import fr.gxfs.edc.extension.s3.validation.MinioSourceDataAddressValidator;
import java.util.Collections;
import java.util.List;
import org.eclipse.edc.spi.types.domain.DataAddress;
import org.eclipse.edc.validator.spi.ValidationResult;
import org.junit.jupiter.api.Test;

public class MinioSourceDataAddressValidatorTest {

  @Test
  public void sourceDataAddressValidatorShouldSucceededWhenPropertiesAreSet() {
    MinioSourceDataAddressValidator validator = new MinioSourceDataAddressValidator();
    DataAddress dataAddress = DataAddress.Builder.newInstance()
        .type(MinioBucketSchema.TYPE)
        .property(MinioBucketSchema.BUCKET_NAME, "testBucket")
        .property(MinioBucketSchema.OBJECT_NAME, "testObject")
        .property(MinioBucketSchema.OBJECT_PREFIX, "testPrefix")
        .build();

    ValidationResult result = validator.validate(dataAddress);

    assertTrue(result.succeeded());
    assertTrue(result.getFailureMessages().isEmpty());
  }

  @Test
  public void sourceDataAddressValidatorShouldFailsWhenBucketNamePropertyNotSet() {
    MinioSourceDataAddressValidator validator = new MinioSourceDataAddressValidator();
    DataAddress dataAddress = DataAddress.Builder.newInstance()
        .type(MinioBucketSchema.TYPE)
        .property(MinioBucketSchema.OBJECT_NAME, "testObject")
        .property(MinioBucketSchema.OBJECT_PREFIX, "testPrefix")
        .build();

    ValidationResult result = validator.validate(dataAddress);

    assertTrue(result.failed());
    assertIterableEquals(result.getFailureMessages(),
        List.of("The field " + MinioBucketSchema.BUCKET_NAME + " is required"));
  }

  @Test
  public void sourceDataAddressValidatorShouldFailsWhenObjectNamePropertyNotSet() {
    MinioSourceDataAddressValidator validator = new MinioSourceDataAddressValidator();
    DataAddress dataAddress = DataAddress.Builder.newInstance()
        .type(MinioBucketSchema.TYPE)
        .property(MinioBucketSchema.BUCKET_NAME, "testBucket")
        .property(MinioBucketSchema.OBJECT_PREFIX, "testPrefix")
        .build();

    ValidationResult result = validator.validate(dataAddress);

    assertTrue(result.failed());
    assertIterableEquals(result.getFailureMessages(),
        List.of("The field " + MinioBucketSchema.OBJECT_NAME + " is required"));
  }

  @Test
  public void sourceDataAddressValidatorShouldSucceedWhenObjectPrefixPropertyNotSet() {
    MinioSourceDataAddressValidator validator = new MinioSourceDataAddressValidator();
    DataAddress dataAddress = DataAddress.Builder.newInstance()
        .type(MinioBucketSchema.TYPE)
        .property(MinioBucketSchema.BUCKET_NAME, "testBucket")
        .property(MinioBucketSchema.OBJECT_NAME, "testObject")
        .build();

    ValidationResult result = validator.validate(dataAddress);

    assertTrue(result.succeeded());
    assertIterableEquals(Collections.emptyList(), result.getFailureMessages());
  }

  @Test
  public void sourceDataAddressValidatorShouldFailsWhenMandatoryPropertiesNotSet() {
    MinioSourceDataAddressValidator validator = new MinioSourceDataAddressValidator();
    DataAddress dataAddress = DataAddress.Builder.newInstance()
        .type(MinioBucketSchema.TYPE)
        .property(MinioBucketSchema.ACCESS_KEY_ID, "testAccessKey")
        .build();

    ValidationResult result = validator.validate(dataAddress);

    assertTrue(result.failed());
    assertIterableEquals(
        result.getFailureMessages(),
        List.of(
            "The field " + MinioBucketSchema.BUCKET_NAME + " is required",
            "The field " + MinioBucketSchema.OBJECT_NAME + " is required"
        )
    );
  }
}
