package fr.gxfs.edc.extension.s3.validator;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema;
import fr.gxfs.edc.extension.s3.validation.MinioCredentialDataAddressValidator;
import java.util.List;
import org.eclipse.edc.spi.types.domain.DataAddress;
import org.eclipse.edc.validator.spi.ValidationResult;
import org.junit.jupiter.api.Test;

public class MinioCredentialDataAddressValidatorTest {

  @Test
  public void credentialDataAddressValidatorShouldSucceededWhenPropertiesAreSet() {
    MinioCredentialDataAddressValidator validator = new MinioCredentialDataAddressValidator();
    DataAddress dataAddress = DataAddress.Builder.newInstance()
        .type(MinioBucketSchema.TYPE)
        .property(MinioBucketSchema.ACCESS_KEY_ID, "testAccessKey")
        .property(MinioBucketSchema.SECRET_ACCESS_KEY, "testSecretKey")
        .property(MinioBucketSchema.ENDPOINT, "testEndpoint")
        .build();

    ValidationResult result = validator.validate(dataAddress);

    assertTrue(result.succeeded());
    assertTrue(result.getFailureMessages().isEmpty());
  }

  @Test
  public void credentialDataAddressValidatorShouldFailsWhenAccessKeyPropertyNotSet() {
    MinioCredentialDataAddressValidator validator = new MinioCredentialDataAddressValidator();
    DataAddress dataAddress = DataAddress.Builder.newInstance()
        .type(MinioBucketSchema.TYPE)
        .property(MinioBucketSchema.SECRET_ACCESS_KEY, "testSecretKey")
        .property(MinioBucketSchema.ENDPOINT, "testEndpoint")
        .build();

    ValidationResult result = validator.validate(dataAddress);

    assertTrue(result.failed());
    assertIterableEquals(result.getFailureMessages(),
        List.of("The field " + MinioBucketSchema.ACCESS_KEY_ID + " is required"));
  }

  @Test
  public void credentialDataAddressValidatorShouldFailsWhenSecretKeyPropertyNotSet() {
    MinioCredentialDataAddressValidator validator = new MinioCredentialDataAddressValidator();
    DataAddress dataAddress = DataAddress.Builder.newInstance()
        .type(MinioBucketSchema.TYPE)
        .property(MinioBucketSchema.ACCESS_KEY_ID, "testAccessKey")
        .property(MinioBucketSchema.ENDPOINT, "testEndpoint")
        .build();

    ValidationResult result = validator.validate(dataAddress);

    assertTrue(result.failed());
    assertIterableEquals(result.getFailureMessages(),
        List.of("The field " + MinioBucketSchema.SECRET_ACCESS_KEY + " is required"));
  }

  @Test
  public void credentialDataAddressValidatorShouldFailsWhenEndpointPropertyNotSet() {
    MinioCredentialDataAddressValidator validator = new MinioCredentialDataAddressValidator();
    DataAddress dataAddress = DataAddress.Builder.newInstance()
        .type(MinioBucketSchema.TYPE)
        .property(MinioBucketSchema.ACCESS_KEY_ID, "testAccessKey")
        .property(MinioBucketSchema.SECRET_ACCESS_KEY, "testEndpoint")
        .build();

    ValidationResult result = validator.validate(dataAddress);

    assertTrue(result.failed());
    assertIterableEquals(result.getFailureMessages(),
        List.of("The field " + MinioBucketSchema.ENDPOINT + " is required"));
  }

  @Test
  public void credentialDataAddressValidatorShouldFailsWhenMandatoryPropertiesNotSet() {
    MinioCredentialDataAddressValidator validator = new MinioCredentialDataAddressValidator();
    DataAddress dataAddress = DataAddress.Builder.newInstance()
        .type(MinioBucketSchema.TYPE)
        .build();

    ValidationResult result = validator.validate(dataAddress);

    assertTrue(result.failed());
    assertIterableEquals(
        result.getFailureMessages(),
        List.of(
            "The field " + MinioBucketSchema.ACCESS_KEY_ID + " is required",
            "The field " + MinioBucketSchema.SECRET_ACCESS_KEY + " is required",
            "The field " + MinioBucketSchema.ENDPOINT + " is required"
        )
    );
  }
}
