package fr.gxfs.edc.extension.s3.validator;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema;
import fr.gxfs.edc.extension.s3.validation.MinioDestinationDataAddressValidator;
import java.util.List;
import org.eclipse.edc.spi.types.domain.DataAddress;
import org.eclipse.edc.validator.spi.ValidationResult;
import org.junit.jupiter.api.Test;

public class MinioDestinationDataAddressValidatorTest {

  @Test
  public void destinationDataAddressValidatorShouldSucceededWhenPropertiesAreSet() {
    MinioDestinationDataAddressValidator validator = new MinioDestinationDataAddressValidator();
    DataAddress dataAddress = DataAddress.Builder.newInstance()
        .type(MinioBucketSchema.TYPE)
        .property(MinioBucketSchema.BUCKET_NAME, "testBucket")
        .build();

    ValidationResult result = validator.validate(dataAddress);

    assertTrue(result.succeeded());
    assertTrue(result.getFailureMessages().isEmpty());
  }

  @Test
  public void destinationDataAddressValidatorShouldFailsWhenBucketNamePropertyNotSet() {
    MinioDestinationDataAddressValidator validator = new MinioDestinationDataAddressValidator();
    DataAddress dataAddress = DataAddress.Builder.newInstance()
        .type(MinioBucketSchema.TYPE)
        .build();

    ValidationResult result = validator.validate(dataAddress);

    assertTrue(result.failed());
    assertIterableEquals(result.getFailureMessages(),
        List.of("The field " + MinioBucketSchema.BUCKET_NAME + " is required"));
  }
}
