package fr.gxfs.edc.provision.minio.s3;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

import org.junit.jupiter.api.Test;

public class MinioConsumerResourceDefinitionTest {

  @Test
  void builderShouldThrowExceptionWhenBucketNameIsNull() {
      assertThatThrownBy(() -> MinioResourceDefinition.Builder.newInstance()
              .id("id")
              .transferProcessId("tp-id")
              .build())
              .isInstanceOf(NullPointerException.class)
              .hasMessage("bucketName is required");
  }

  @Test
  void toBuilderShouldReturnsTheSameObject() {
      var definition = MinioResourceDefinition.Builder.newInstance()
              .id("id")
              .transferProcessId("tp-id")
              .bucketName("bucket")
              .build();

      var builder = definition.toBuilder();
      var rebuiltDefinition = builder.build();

      assertThat(rebuiltDefinition).usingRecursiveComparison().isEqualTo(definition);
  }
}
