package fr.gxfs.edc.provision.minio.s3;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.UUID;
import java.util.stream.Stream;
import fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema;
import org.eclipse.edc.connector.controlplane.asset.spi.domain.Asset;
import org.eclipse.edc.connector.controlplane.transfer.spi.types.TransferProcess;
import org.eclipse.edc.policy.model.Policy;
import org.eclipse.edc.spi.types.domain.DataAddress;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.aggregator.AggregateWith;
import org.junit.jupiter.params.aggregator.ArgumentsAccessor;
import org.junit.jupiter.params.aggregator.ArgumentsAggregationException;
import org.junit.jupiter.params.aggregator.ArgumentsAggregator;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullSource;

class MinioConsumerResourceDefinitionGeneratorTest {
  private MinioConsumerResourceDefinitionGenerator generator;

  @BeforeEach
  void setUp() {
      generator = new MinioConsumerResourceDefinitionGenerator();
  }

  @ParameterizedTest
  @MethodSource("provideValidBucketNames")
  public void canGenerateShouldReturnValidResourceDefinition(@AggregateWith(TransferProcessAggregator.class) TransferProcess transferProcess) {
        var policy = Policy.Builder.newInstance().build();

        assertTrue(generator.canGenerate(transferProcess, policy));
  }

  @ParameterizedTest
  @NullSource
  public void canGenerateShouldThrowNullPointerExceptionIfTransferProcessIsNull(TransferProcess transferProcess) {
    var policy = Policy.Builder.newInstance().build();
    assertThatExceptionOfType(NullPointerException.class).isThrownBy(() -> generator.canGenerate(transferProcess, policy));
  }

  @ParameterizedTest
  @NullSource
  public void canGenerateShouldThrowNullPointerExceptionIfPolicyIsNull(Policy policy) {
    var destination = DataAddress.Builder.newInstance().type(MinioBucketSchema.TYPE)
        .property(MinioBucketSchema.BUCKET_NAME, "bucket")
        .build();

    var asset = Asset.Builder.newInstance().build();
    var transferProcess = TransferProcess.Builder.newInstance()
            .dataDestination(destination)
            .assetId(asset.getId())
            .correlationId("process-id")
            .build();
    assertThatExceptionOfType(NullPointerException.class).isThrownBy(() -> generator.canGenerate(transferProcess, policy));
  }

  @ParameterizedTest
  @MethodSource("provideValidBucketNames")
  public void generateShouldReturnValidResourceDefinition(@AggregateWith(TransferProcessAggregator.class) TransferProcess transferProcess) {
        var policy = Policy.Builder.newInstance().build();

        var definition = generator.generate(transferProcess, policy);
        assertThat(definition).isInstanceOf(MinioResourceDefinition.class);

        var objectDef = (MinioResourceDefinition) definition;
        assertThat(objectDef.getBucketName()).isEqualTo(transferProcess.getDataDestination().getStringProperty(MinioBucketSchema.BUCKET_NAME));
        assertThat(objectDef.getId()).satisfies(UUID::fromString);
  }

  @ParameterizedTest
  @NullSource
  public void generateShouldThrowNullPointerExceptionIfTransferProcessIsNull(TransferProcess transferProcess) {
    var policy = Policy.Builder.newInstance().build();
    assertThatExceptionOfType(NullPointerException.class).isThrownBy(() -> generator.generate(transferProcess, policy));
  }

  @ParameterizedTest
  @NullSource
  public void generateShouldThrowNullPointerExceptionIfPolicyIsNull(Policy policy) {
    var destination = DataAddress.Builder.newInstance().type(MinioBucketSchema.TYPE)
        .property(MinioBucketSchema.BUCKET_NAME, "bucket")
        .build();

    var asset = Asset.Builder.newInstance().build();
    var transferProcess = TransferProcess.Builder.newInstance()
            .dataDestination(destination)
            .assetId(asset.getId())
            .correlationId("process-id")
            .build();
    assertThatExceptionOfType(NullPointerException.class).isThrownBy(() -> generator.generate(transferProcess, policy));
  }

  private static @NotNull Stream<Arguments> provideValidBucketNames() {
    return Stream.of(
        Arguments.of("default-bucket"),
        Arguments.of("default-bucket"),
        Arguments.of("bucket"),
        Arguments.of("default-bucket")
    );
  }
}

class TransferProcessAggregator implements ArgumentsAggregator {
    @Override
    public Object aggregateArguments(@NotNull ArgumentsAccessor accessor, ParameterContext context)
      throws ArgumentsAggregationException {
        var destination = DataAddress.Builder.newInstance().type(MinioBucketSchema.TYPE)
                .property(MinioBucketSchema.BUCKET_NAME, accessor.getString(0))
                .build();

        var asset = Asset.Builder.newInstance().build();
        return TransferProcess.Builder.newInstance()
                .dataDestination(destination)
                .assetId(asset.getId())
                .correlationId("process-id")
                .build();
    }
}