package fr.gxfs.edc.provision.minio.s3;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema;
import org.eclipse.edc.json.JacksonTypeManager;
import org.junit.jupiter.api.Test;

class MinioBucketProvisionedResourceTest {

  private final ObjectMapper mapper = new JacksonTypeManager().getMapper();

  @Test
  void deserializeShouldGetValidObjectWhenJsonStringIsValid() throws IOException {
    String jsonString ="{\"edctype\":\"dataspaceconnector:miniobucketprovisionedresource\",\"id\":\"37dee866-5001-4a5f-ad9e-bd0a54cc5a94\",\"transferProcessId\":\"123\",\"resourceDefinitionId\":\"4e421ee6-4df5-4616-b106-89a160918d2e\",\"resourceName\":\"bucket\",\"dataAddress\":{\"properties\":{\"bucketName\":\"bucket\",\"https://w3id.org/edc/v0.0.1/ns/type\":\"Minio\",\"region\":\"gra\"}},\"region\":\"gra\",\"bucketName\":\"bucket\"}";

    MinioBucketProvisionedResource provisionedResource = mapper.readValue(jsonString,
        MinioBucketProvisionedResource.class);

    assertNotNull(provisionedResource);
    assertEquals("bucket", provisionedResource.getBucketName());
    assertEquals("bucket", provisionedResource.getResourceName());
    assertEquals("37dee866-5001-4a5f-ad9e-bd0a54cc5a94", provisionedResource.getId());
    assertEquals("123", provisionedResource.getTransferProcessId());
    assertEquals("4e421ee6-4df5-4616-b106-89a160918d2e", provisionedResource.getResourceDefinitionId());

    var dataAddress = provisionedResource.getDataAddress();
    assertNotNull(dataAddress);
    assertEquals("Minio", dataAddress.getType());
    assertEquals("bucket", dataAddress.getStringProperty(MinioBucketSchema.BUCKET_NAME));
  }

  @Test
  void deserializeShouldThrowErrorWhenJsonStringIsNull() {
    assertThrows(IllegalArgumentException.class, () -> {
      mapper.readValue((File) null, MinioBucketProvisionedResource.class);
    });
  }

  @Test
  void deserializeShouldThrowErrorWhenJsonStringIsEmpty() {
    assertThrows(JsonMappingException.class, () -> {
      mapper.readValue("", MinioBucketProvisionedResource.class);
    });
  }

  @Test
  void deserializeShouldThrowErrorWhenJsonStringIsInvalid() {
    assertThrows(JsonParseException.class, () -> {
      mapper.readValue("invalid json", MinioBucketProvisionedResource.class);
    });
  }

  @Test
  void deserializeShouldThrowErrorWhenBucketNamePropertyIsMissing() {
    String jsonString = "{\"test\":\"fails\"}"; // bucketName property is missing

    assertThrows(JsonMappingException.class, () -> {
      mapper.readValue(jsonString, MinioBucketProvisionedResource.class);
    });
  }
}
