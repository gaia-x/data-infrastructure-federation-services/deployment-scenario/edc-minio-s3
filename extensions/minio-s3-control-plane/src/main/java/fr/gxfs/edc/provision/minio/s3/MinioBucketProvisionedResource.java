package fr.gxfs.edc.provision.minio.s3;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.eclipse.edc.connector.controlplane.transfer.spi.types.ProvisionedDataDestinationResource;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import static fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema.BUCKET_NAME;
import static fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema.OBJECT_NAME;
import static fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema.OBJECT_PREFIX;
import static fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema.PATH;
import static fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema.TYPE;

/**
 * Represents a provisioned resource for an Minio S3 bucket. It contains the region and the bucket
 * name.
 */
@JsonDeserialize(builder = MinioBucketProvisionedResource.Builder.class)
@JsonTypeName("dataspaceconnector:miniobucketprovisionedresource")
public class MinioBucketProvisionedResource extends ProvisionedDataDestinationResource {


  /**
   * Returns the name of the bucket.
   *
   * @return the name of the bucket.
   */
  public String getBucketName() {
    return getDataAddress().getStringProperty(BUCKET_NAME);
  }

  /**
   * Returns the name of the resource.
   *
   * @return the name of the resource.
   */
  @Override
  public String getResourceName() {
    return getBucketName();
  }

  private MinioBucketProvisionedResource() {
  }

  /**
   * Builder for {@link MinioBucketProvisionedResource}.
   */
  @JsonPOJOBuilder(withPrefix = "")
  public static class Builder extends
      ProvisionedDataDestinationResource.Builder<MinioBucketProvisionedResource, Builder> {

    private Builder() {
      super(new MinioBucketProvisionedResource());
      dataAddressBuilder.type(TYPE);
    }


    /**
     * Creates a new instance of the builder.
     *
     * @return a new instance of the builder.
     */
    @Contract(" -> new")
    @JsonCreator
    public static @NotNull Builder newInstance() {
      return new Builder();
    }

    /**
     * Sets the name of the bucket.
     *
     * @param bucketName the name of the bucket.
     * @return the builder.
     */
    public Builder bucketName(String bucketName) {
      dataAddressBuilder.property(BUCKET_NAME, bucketName);
      return this;
    }

    public Builder objectName(String objectName) {
      dataAddressBuilder.property(OBJECT_NAME, objectName);
      return this;
    }

    public Builder path(String path) {
      dataAddressBuilder.property(PATH, path);
      return this;
    }
  }
}
