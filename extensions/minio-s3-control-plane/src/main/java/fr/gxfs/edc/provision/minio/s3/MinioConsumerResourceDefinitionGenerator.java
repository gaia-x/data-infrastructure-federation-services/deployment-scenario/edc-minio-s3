package fr.gxfs.edc.provision.minio.s3;

import static java.util.UUID.randomUUID;

import java.util.Objects;
import java.util.Optional;
import fr.gxfs.edc.extension.s3.schemas.MinioBucketSchema;
import org.eclipse.edc.connector.controlplane.transfer.spi.provision.ConsumerResourceDefinitionGenerator;
import org.eclipse.edc.connector.controlplane.transfer.spi.types.ResourceDefinition;
import org.eclipse.edc.connector.controlplane.transfer.spi.types.TransferProcess;
import org.eclipse.edc.policy.model.Policy;
import org.jetbrains.annotations.Nullable;
import javax.swing.text.html.Option;

/**
 * A {@link ConsumerResourceDefinitionGenerator} implementation for Minio S3 buckets.
 */
public class MinioConsumerResourceDefinitionGenerator implements
    ConsumerResourceDefinitionGenerator {

  /**
   * Generates a {@link ResourceDefinition} for the given {@link TransferProcess} and
   * {@link Policy}.
   *
   * @param transferProcess The {@link TransferProcess} for which to generate the
   *                        {@link ResourceDefinition}.
   * @param policy          The {@link Policy} for which to generate the
   *                        {@link ResourceDefinition}.
   * @return The generated {@link ResourceDefinition}.
   */
  @Override
  public @Nullable ResourceDefinition generate(TransferProcess transferProcess, Policy policy) {
    Objects.requireNonNull(transferProcess, "transferProcess must always be provided");
    Objects.requireNonNull(policy, "policy must always be provided");

    var destination = transferProcess.getDataDestination();
    var id = randomUUID().toString();
    var bucketName = destination.getStringProperty(MinioBucketSchema.BUCKET_NAME);

    var builder =  MinioResourceDefinition.Builder.newInstance()
        .id(id)
        .bucketName(bucketName);

    if (destination.hasProperty(MinioBucketSchema.OBJECT_NAME)) {
      builder.objectName(Optional.ofNullable(destination.getStringProperty(MinioBucketSchema.OBJECT_NAME)));
    } else {
      builder.objectName(Optional.empty());
    }

    if (destination.hasProperty(MinioBucketSchema.PATH)) {
      builder.path(Optional.ofNullable(destination.getStringProperty(MinioBucketSchema.PATH)));
    } else {
      builder.path(Optional.empty());
    }

    return builder.build();

  }


  /**
   * Determines whether this generator can generate a {@link ResourceDefinition} for the given
   * {@link TransferProcess} and {@link Policy}.
   *
   * @param transferProcess The {@link TransferProcess} for which to determine whether a
   *                        {@link ResourceDefinition} can be generated. Must not be {@code null}.
   * @param policy          The {@link Policy} for which to determine whether a
   *                        {@link ResourceDefinition} can be generated. Must not be {@code null}.
   * @return {@code true} if this generator can generate a {@link ResourceDefinition} for the given
   * {@link TransferProcess} and {@link Policy}, {@code false} otherwise.
   * @throws NullPointerException if {@code transferProcess} is {@code null} or if {@code policy} is
   *                              {@code null}.
   */
  @Override
  public boolean canGenerate(TransferProcess transferProcess, Policy policy)
      throws NullPointerException {
    Objects.requireNonNull(transferProcess, "transferProcess must always be provided");
    Objects.requireNonNull(policy, "policy must always be provided");

    return MinioBucketSchema.TYPE.equals(transferProcess.getDestinationType());
  }
}
