package fr.gxfs.edc.provision.minio.s3;

import static dev.failsafe.Failsafe.with;

import dev.failsafe.RetryPolicy;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import fr.gxfs.edc.extension.s3.api.MinioConnectorAPI;
import org.eclipse.edc.connector.controlplane.transfer.spi.provision.Provisioner;
import org.eclipse.edc.connector.controlplane.transfer.spi.types.DeprovisionedResource;
import org.eclipse.edc.connector.controlplane.transfer.spi.types.ProvisionResponse;
import org.eclipse.edc.connector.controlplane.transfer.spi.types.ProvisionedResource;
import org.eclipse.edc.connector.controlplane.transfer.spi.types.ResourceDefinition;
import org.eclipse.edc.policy.model.Policy;
import org.eclipse.edc.spi.monitor.Monitor;
import org.eclipse.edc.spi.response.StatusResult;
import org.jetbrains.annotations.NotNull;

/**
 * A {@link Provisioner} implementation for Minio S3 buckets.
 */
public class MinioProvisioner implements
    Provisioner<MinioResourceDefinition, MinioBucketProvisionedResource> {

  private final RetryPolicy<Object> retryPolicy;
  private final Monitor monitor;
  private final MinioConnectorAPI minioClient;

  /**
   * Creates a new {@link MinioProvisioner}.
   * 
   * @param retryPolicy      the {@link RetryPolicy} to use.
   * @param monitor          the {@link Monitor} to use.
   * @param minioClient the {@link fr.gxfs.edc.extension.s3.api.MinioConnectorAPI} to use.
   */
  public MinioProvisioner(RetryPolicy<Object> retryPolicy, Monitor monitor,
      MinioConnectorAPI minioClient) {
    this.retryPolicy = retryPolicy;
    this.monitor = monitor;
    this.minioClient = minioClient;
  }

  /**
   * Determines whether this provisioner can provision a
   * {@link ResourceDefinition}.
   * 
   * @param resourceDefinition the {@link ResourceDefinition} to check.
   * @return {@code true} if this provisioner can provision the
   *         {@link ResourceDefinition}.
   */
  @Override
  public boolean canProvision(ResourceDefinition resourceDefinition) {
    return resourceDefinition instanceof MinioResourceDefinition;
  }

  /**
   * Determines whether this provisioner can deprovision a
   * {@link ProvisionedResource}.
   * 
   * @param provisionedResource the {@link ProvisionedResource} to check.
   * @return {@code true} if this provisioner can deprovision the
   *         {@link ProvisionedResource}.
   */
  @Override
  public boolean canDeprovision(ProvisionedResource provisionedResource) {
    return provisionedResource instanceof MinioBucketProvisionedResource;
  }

  /**
   * Provisions a new Minio S3 bucket. If the bucket already exists, it will be
   * reused.
   * 
   * @param minioResourceDefinition the {@link MinioResourceDefinition}
   *                                     to provision.
   * @param policy                       the {@link Policy} to use.
   * @return a {@link CompletableFuture} that will complete with the
   *         {@link ProvisionResponse}.
   */
  @Override
  public CompletableFuture<StatusResult<ProvisionResponse>> provision(
      MinioResourceDefinition minioResourceDefinition, Policy policy) {

    var bucketName = minioResourceDefinition.getBucketName();

    Optional.ofNullable(monitor)
        .ifPresent(m -> m.info("Provisioning request submitted for bucket: " + bucketName));

    return with(retryPolicy)
        .getAsync(() -> minioClient.bucketExists(bucketName))
        .thenCompose(exists -> {
          if (exists) {
            return reuseExistingBucket(bucketName);
          } else {
            return createBucket(bucketName);
          }
        })
        .thenApply(empty -> {
          var resourceName = minioResourceDefinition.getObjectName() + "-" + OffsetDateTime.now();

          Optional.ofNullable(monitor)
              .ifPresent(m -> {
                m.debug("MinioProvisioner: provisioned bucket " + bucketName);
                m.debug("MinioProvisioner: with resource name " + resourceName);
              });

          var resourceBuilder = MinioBucketProvisionedResource.Builder.newInstance()
              .id(minioResourceDefinition.getId())
              .bucketName(bucketName)
              .resourceDefinitionId(minioResourceDefinition.getId())
              .transferProcessId(minioResourceDefinition.getTransferProcessId())
              .resourceName(resourceName);

          if (minioResourceDefinition.getPath().isPresent()) {
            resourceBuilder.path(minioResourceDefinition.getPath().get());
          }

          if (minioResourceDefinition.getObjectName().isPresent()) {
            resourceBuilder.objectName(minioResourceDefinition.getObjectName().get());
          }

          var response = ProvisionResponse.Builder.newInstance().resource(resourceBuilder.build()).build();
          return StatusResult.success(response);
        });
  }

  /**
   * Deprovisions an Minio S3 bucket. The bucket will be deleted.
   * 
   * @param minioBucketProvisionedResource the
   *                                            {@link MinioBucketProvisionedResource}
   *                                            to deprovision.
   * @param policy                              the {@link Policy} to use.
   * @return a {@link CompletableFuture} that will complete with the
   *         {@link DeprovisionedResource}.
   */
  @Override
  public CompletableFuture<StatusResult<DeprovisionedResource>> deprovision(
      MinioBucketProvisionedResource minioBucketProvisionedResource, Policy policy) {

    var bucketName = minioBucketProvisionedResource.getBucketName();
    Optional.ofNullable(monitor)
        .ifPresent(m -> m.info("Deprovisioning request submitted for bucket: " + bucketName));

    return with(retryPolicy)
        .getAsync(() -> minioClient.listObjects(bucketName, ""))
        .thenCompose((listObjectsResponse) -> deleteObjects(bucketName, listObjectsResponse))
        .thenApply(empty -> {
          minioClient.deleteBucket(bucketName);
          return StatusResult.success(DeprovisionedResource.Builder.newInstance()
              .provisionedResourceId(minioBucketProvisionedResource.getId())
              .build());
        });
  }

  @NotNull
  private CompletableFuture<Void> reuseExistingBucket(String bucketName) {
    Optional.ofNullable(monitor).ifPresent(m -> m.info("Reusing existing bucket " + bucketName));
    return CompletableFuture.completedFuture(null);
  }

  @NotNull
  private CompletableFuture<Void> createBucket(String bucketName) {
    return with(retryPolicy)
        .runAsync(() -> {
          minioClient.createBucket(bucketName);
          Optional.ofNullable(monitor).ifPresent(
              m -> m.debug("MinioProvisioner: created a new container " + bucketName));
        });
  }

  @NotNull
  private CompletableFuture<List<String>> listObjects(String bucketName, String prefix) {
    return with(retryPolicy).getAsync(() -> minioClient.listObjects(bucketName, prefix));
  }

  @NotNull
  private CompletableFuture<Void> deleteObjects(String bucketName, List<String> keys) {
    return with(retryPolicy).runAsync(() -> minioClient.deleteObjects(bucketName, keys));
  }

  @NotNull
  private CompletableFuture<Void> deleteBucket(String bucketName) {
    return with(retryPolicy).runAsync(() -> minioClient.deleteBucket(bucketName));
  }
}
