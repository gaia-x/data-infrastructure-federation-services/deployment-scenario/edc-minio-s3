package fr.gxfs.edc.provision.minio;

import dev.failsafe.RetryPolicy;
import java.util.Optional;
import fr.gxfs.edc.extension.s3.api.MinioConnectorAPI;
import fr.gxfs.edc.provision.minio.s3.MinioBucketProvisionedResource;
import fr.gxfs.edc.provision.minio.s3.MinioConsumerResourceDefinitionGenerator;
import fr.gxfs.edc.provision.minio.s3.MinioProvisioner;
import fr.gxfs.edc.provision.minio.s3.MinioResourceDefinition;
import org.eclipse.edc.connector.controlplane.transfer.spi.provision.ProvisionManager;
import org.eclipse.edc.connector.controlplane.transfer.spi.provision.Provisioner;
import org.eclipse.edc.connector.controlplane.transfer.spi.provision.ResourceManifestGenerator;
import org.eclipse.edc.runtime.metamodel.annotation.Extension;
import org.eclipse.edc.runtime.metamodel.annotation.Inject;
import org.eclipse.edc.spi.monitor.Monitor;
import org.eclipse.edc.spi.security.Vault;
import org.eclipse.edc.spi.system.ServiceExtension;
import org.eclipse.edc.spi.system.ServiceExtensionContext;
import org.eclipse.edc.spi.types.TypeManager;

/**
 * Provides data transfer {@link Provisioner}s backed by Minio services.
 */
@Extension(value = MinioProvisionExtension.NAME)
public class MinioProvisionExtension implements ServiceExtension {

  /**
   * The name of this service extension.
   */
  public static final String NAME = "Control plane - Minio";

  /**
   * The {@link Vault} service.
   */
  @Inject
  private Vault vault;

  /**
   * The {@link Monitor} service.
   */
  @Inject
  private Monitor monitor;

  /**
   * The {@link TypeManager} service.
   */
  @Inject
  private TypeManager typeManager;

  /**
   * The {@link MinioConnectorAPI} service.
   */
  @Inject
  private MinioConnectorAPI client;

  /**
   * The name of the {@link Provisioner} service.
   *
   * @return the name of this service extension.
   */
  @Override
  public String name() {
    return NAME;
  }

  /**
   * Initializes this service extension.
   *
   * @param context the service extension context.
   */
  @Override
  public void initialize(ServiceExtensionContext context) {
    monitor = context.getMonitor();

    Optional.ofNullable(monitor)
        .ifPresent(
            m -> m.debug("Initializing Minio Provision extension"));

    var provisionManager = context.getService(ProvisionManager.class);
    var retryPolicy = context.getService(RetryPolicy.class);
    var bucketProvisioner = new MinioProvisioner(retryPolicy, monitor, client);

    Optional.ofNullable(monitor)
        .ifPresent(
            m -> {
              m.debug("Registering Minio Provisioner with ProvisionManager");
              m.debug("Registering Minio Provisioner with RetryPolicy");
              m.debug("Registering Minio Provisioner with MinioProvisioner");
            });
    provisionManager.register(bucketProvisioner);

    var manifestGenerator = context.getService(ResourceManifestGenerator.class);
    manifestGenerator.registerGenerator(new MinioConsumerResourceDefinitionGenerator());

    typeManager.registerTypes(MinioBucketProvisionedResource.class,
        MinioResourceDefinition.class);
  }

}
