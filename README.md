# Minio S3 Extension for Eclipse Dataspace Connector

This repository contains the Minio S3 Extension that works with the Eclipse Dataspace Connector 
allowing operations into the Minio S3 Storage.

## Based on the following

- [https://github.com/eclipse-edc/Connector](https://github.com/eclipse-edc/Connector) - v0.6.2;
- [International Data Spaces](https://www.internationaldataspaces.org);
- [GAIA-X](https://gaia-x.eu) project;
- [Minio S3](https://min.io/).

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[Apache v2.0](https://choosealicense.com/licenses/apache-2.0/)
